export function addImage(image) {
  return {
    type: 'ADD_IMAGE',
    payload: image,
  };
}

export function removeImage(id) {
  return {
    type: 'REMOVE_IMAGE',
    payload: { id },
  };
}

export function moveImage(oldIndex, newIndex) {
  return {
    type: 'MOVE_IMAGE',
    payload: { oldIndex, newIndex },
  };
}