import { combineReducers } from 'redux';
import arrayMove from '../helpers/arrayMove';

const images = (state = [], { type, payload }) => {
  switch (type) {
    case 'ADD_IMAGE':
      return [...state, payload];

    case 'REMOVE_IMAGE':
      return state.filter((image) => image.id !== payload.id);

    case 'MOVE_IMAGE':
      return arrayMove(state, payload.oldIndex, payload.newIndex);

    default:
      return [...state];
  }
};

export default combineReducers({ images });
