import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { addImage, removeImage, moveImage } from './redux/actions';
import getBase64 from './helpers/getBase64';

import './App.css';

const SortableItem = SortableElement(({ value }) => (
  <div className="image-card">
    <img src={value.base64} alt="image" draggable={false} />
    <span className="index">
      {`Current id: ${value.id}`}
    </span>
  </div>
));

const SortableList = SortableContainer(({ items }) => (
  <div className="images-gallery">
    {
      items.map((e, i) => <SortableItem key={i} value={e} index={i} />)
    }
  </div>
));

class App extends Component {
  state = {
    id: 1,
  };

  uploadImage = (e) => {
    const { id } = this.state;
    const { files } = e.target;
    const filesCount = files.length;

    for (let i = 0; i < filesCount; i++) {
      const file = files.item(i);

      getBase64(file).then((base64) => {
        this.props.addImage({
          id: id + i,
          base64,
          file,
        });
      });
    }

    this.setState((prevState) => ({ id: prevState.id + filesCount }));
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.props.moveImage(oldIndex, newIndex);
  };

  render() {
    const { images } = this.props;

    return (
      <div className="App">
        <SortableList items={images} onSortEnd={this.onSortEnd} />
        <input type="file" multiple onChange={this.uploadImage} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  images: state.images,
});

const mapDispatchToProps = (dispatch) => ({
  addImage: (image) => dispatch(addImage(image)),
  removeImage: (id) => dispatch(removeImage(id)),
  moveImage: (oldIndex, newIndex) => dispatch(moveImage(oldIndex, newIndex)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
