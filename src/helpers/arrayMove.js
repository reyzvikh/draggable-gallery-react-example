function arrayMoveMutate(array, from, to) {
  array.splice((to < 0 ? array.length + to : to), 0, array.splice(from, 1)[0]);
}

export default function arrayMove(array, from, to) {
  const copy = [...array];
  arrayMoveMutate(copy, from, to);

  return copy;
}